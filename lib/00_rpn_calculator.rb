class RPNCalculator
  attr_accessor :stack

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def plus
    if @stack.length >= 2
      @stack << @stack.pop(2).reduce(:+)
    else
      raise "calculator is empty"
    end
  end

  def minus
    if @stack.length >= 2
      @stack << @stack.pop(2).reduce(:-)
    else
      raise "calculator is empty"
    end
  end

  def divide
    if @stack.length >= 2
      @stack << @stack.pop(2).reduce { |a, b| a.to_f / b }
    else
      raise "calculator is empty"
    end
  end

  def times
    if @stack.length >= 2
      @stack << @stack.pop(2).reduce(:*).to_f
    else
      raise "calculator is empty"
    end
  end

  def value
    @stack.last
  end

  def tokens(string)
    numbers = "0123456789"
    res = string.split(" ")

    res.map do |ele|
      if numbers.include?(ele)
        ele.to_i
      else
        operator(ele)
      end
    end
  end

  def operator(string)
    case string
    when "+"
      :+
    when "-"
      :-
    when "*"
      :*
    when "/"
      :/
    end
  end

  def evaluate(string)
    res = []
    equation = tokens(string)

    equation.each do |ele|
      if ele.is_a? Numeric
        res << ele
      elsif ele == :+
        res << res.pop(2).reduce(:+)
      elsif ele == :-
        res << res.pop(2).reduce(:-)
      elsif ele == :/
        res << res.pop(2).reduce { |a, b| a.to_f / b }
      else
        res << res.pop(2).reduce(:*)
      end
    end

    res.last
  end
end
